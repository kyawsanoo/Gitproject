<!-- File: /app/View/Posts/index.ctp -->


<marquee><h1><b>Topic Blogs</b></h1></marquee>
<p><?php echo $this->Html->link('Add Topic', array('action' => 'add')); ?></p>
<p><?php echo $this->Html->link('Logout', array('controller' => 'users' , 'action' => 'logout')); ?></p>
<?php echo $this->Form->create('Search',array('controller' => 'topics', 'url' => 'search')); ?>
<table>
    <tr>
    <td> <?php echo $this->Form->input('SearchId'); ?> </td>
    <td> <?php echo $this->Form->input('SearchTopicName'); ?> </td>
    </tr>
</table>
<?php echo $this->Form->end(__('Search')); ?>
<table>
    <tr>
        <th>Id</th>
        <th>Topic</th>
        <th>Actions</th>
        <th>Created</th>
    </tr>

<!-- Here's where we loop through our $posts array, printing out post info -->
   
    <?php  foreach ($topics as $topic): ?>
     <tr>
        <td><?php echo $topic['Topic']['id']; ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $topic['Topic']['Name'],
                    array('action' => 'view', $topic['Topic']['id'])
                );
            ?>
        </td>
        <td>
            <?php
                if($id == $topic['Topic']['user_id']){
                   echo $this->Form->postLink(
                     'Delete',
                      array('action' => 'delete', $topic['Topic']['id']),
                      array('confirm' => 'Are you sure?')
                   );
                }
            ?>
            <?php
                 if($id == $topic['Topic']['user_id']){
                    echo $this->Html->link(
                    'Edit', array('action' => 'edit', $topic['Topic']['id'])
                     );
                 }
            ?>
        </td>
        <td>
            <?php echo $topic['Topic']['created']; ?>
        </td>
     </tr>
     <?php endforeach; ?>
</table>
<?php echo $this->Paginator->numbers(array(
  'modulus' => 4,   /* Controls the number of page links to display */
  'first' => '< First',
  'last' => 'Last >',
  'before' => ' ', 'after' => ' ')
); ?>