<?php echo $this->Html->link('Home', array('action' => 'index')); ?></p>
<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Actions</th>
        <th>Created</th>
    </tr>

<!-- Here's where we loop through our $posts array, printing out post info -->
    
    <?php foreach ($results as $result): ?>
     <tr>
        <td><?php echo $result['Topic']['id']; ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $result['Topic']['Name'],
                    array('action' => 'view', $result['Topic']['id'])
                );
            ?>
        </td>
        <td>
            <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $result['Topic']['id']),
                    array('confirm' => 'Are you sure?')
                );
            ?>
            <?php
                echo $this->Html->link(
                    'Edit', array('action' => 'edit', $result['Topic']['id'])
                );
            ?>
        </td>
        <td>
            <?php echo $result['Topic']['created']; ?>
        </td>
     </tr>
     <?php endforeach; ?>
</table>