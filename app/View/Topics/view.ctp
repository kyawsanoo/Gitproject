<!-- File: /app/View/Posts/view.ctp -->

<h1><?php echo h($topics['Topic']['Name']); ?></h1>
<p><center><?php echo $this->Html->image($topics['Topic']['photo'],array('width' => '500px','height' => '300px')); ?></center> </p>
<p><small>Created: <?php echo $topics['Topic']['created']; ?></small></p>
<hr>
<table>
<?php foreach ( $comments as $comment): ?>
	   
	   <tr>
	        <td> <p> <?php echo $comment['Comment']['incomment']; ?> </p>
                     <?php echo $this->Html->image($comment['Comment']['image'],array('width' => '200px','height' => '100px')); ?>
	        </td>

	        <td> <?php if($id == $comment['Comment']['user_id']){
	                  echo $this->Form->postLink('Delete',array('action' => 'delete_comment', $comment['Comment']['id'],
	                  $comment['Comment']['topic_id']),array('confirm' => 'Are you sure?'));} ?>
            </td>
            <td> <?php if($id == $comment['Comment']['user_id']){
            	                         echo $this->Html->link(
                                         'Edit', array('action' => 'edit_comment', $comment['Comment']['id']));} ?> 
            </td>
        </tr>
<?php endforeach; ?>
</table>
<h1>Add Comment</h1>
<?php
/*echo $this->Form->create('Comment',array('url' => array('controller' => 'topics', 'action' => 'comment')));
echo $this->Form->input('comment',array('rows' => '3'));
echo $this->Form->input('topic_id',array('type' => 'hidden','value' => $topics['Topic']['id']));
echo $this->Form->end('Add comment');*/
echo $this->Form->create('Comment', array('enctype' => 'multipart/form-data','url' => array('controller' => 'topics','action' => 'comment')));
//echo $this->Form->input('comment',array('rows' => '3'));
echo $this->Form->input('incomment', array('rows' => '3'));
echo $this->Form->input('image',array('type' => 'file'));
echo $this->Form->input('topic_id',array('type' => 'hidden','value' => $topics['Topic']['id']));
echo $this->Form->end('Add Comment');
?>
