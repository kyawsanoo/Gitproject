<?php
class TopicsController extends AppController {
    public $helpers = array('Html', 'Form');
    public $components = array('Paginator');
    var $uses = array('Topic','Comment');



    public function index() {
        //$this->set('posts', $this->Post->find('all'));
        $this->Paginator->settings = array('limit' => 5 );

    // similar to findAll(), but fetches paged results
    $data = $this->Paginator->paginate();
    $this->set('topics',$data);
    $name=$this->Auth->user('username');
    $id=$this->Auth->user('id');
    $this->set('username',$name);
    $this->set('id',$id);
    }
     public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Topic->findById($id);
        $comments = $this->Comment->find('all', array('conditions' => array('Comment.topic_id LIKE' => "%". $id ."%")));
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
         $name=$this->Auth->user('username');
         $id=$this->Auth->user('id');
         $this->set('username',$name);
         $this->set('id',$id);
         $this->set('topics', $post);
         $this->set('comments',$comments);
    }
     public function add() {
        if ($this->request->is('post')) {
            $this->Topic->create();
            $this->request->data['Topic']['user_id'] = $this->Auth->user('id');
            if(!empty($this->data))

                {
                    //Check if image has been uploaded
                    if(!empty($this->request->data['Topic']['photo']['name']))
                    {
                        $file = $this->request->data['Topic']['photo']; //put the data into a var for easy use

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                        $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions

                        //only process if the extension is valid
                        if(in_array($ext, $arr_ext))
                        {
                            //do the actual uploading of the file. First arg is the tmp name, second arg is
                            //where we are putting it
                            move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/' . $file['name']);

                            //prepare the filename for database entry
                            $this->request->data['Topic']['photo'] = '/img/'.$file['name'];
                        }
                    } else{$this->request->data['Topic']['photo'] = null;}
                }        
            if ($this->Topic->save($this->request->data)) {
                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to add your post.'));
        }
    }
    public function edit($id =null) {

    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $topics = $this->Topic->findById($id);
    if (!$topics) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Topic->id = $id;
        if(!empty($this->data))
                {
                    //Check if image has been uploaded
                    if(!empty($this->request->data['Topic']['photo']['name']))
                    {
                        $file = $this->request->data['Topic']['photo']; //put the data into a var for easy use

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                        $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions

                        //only process if the extension is valid
                        if(in_array($ext, $arr_ext))
                        {
                            //do the actual uploading of the file. First arg is the tmp name, second arg is
                            //where we are putting it
                            move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/' . $file['name']);

                            //prepare the filename for database entry
                            $this->request->data['Topic']['photo'] = '/img/'.$file['name'];
                        }
                    }else{$this->request->data['Topic']['photo'] = null;}
                }        
        if ($this->Topic->save($this->request->data)) {
            $this->Flash->success(__('Your post has been updated.'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error(__('Unable to update your post.'));
    }

    if (!$this->request->data) {
        $this->request->data = $topics;
    }
}
public function delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Topic->delete($id)) {
        $this->Flash->success(
            __('The topic with id: %s has been deleted.', h($id))
        );
    } else {
        $this->Flash->error(
            __('The topic with id: %s could not be deleted.', h($id))
        );
    }

    return $this->redirect(array('action' => 'index'));
}
public function search() {
    $id=$this->request->data['Search']['SearchId'];
    $title=$this->request->data['Search']['SearchTopicName'];
    if($id == "" && $title == ""){
       $this->Flash->error(__('Please fill you want to search'));
       return $this->redirect(array('action' => 'index'));
    }
    else if($id !="" && $title == ""){
           $results = $this->Topic->find('all', array(
           'conditions' => array('Topic.id LIKE' => "%". $id ."%")));
    }
    else if($id == "" && $title != ""){
           $results = $this->Topic->find('all', array(
           'conditions' => array('Topic.Name LIKE' => "%". $title ."%")));       
    }
    else{
        $this->Flash->error(__('Please fill only one you want to search'));
        return $this->redirect(array('action' => 'index'));
    }
    $this->set('results',$results);
}
public function comment() {
        if ($this->request->is('post')) {
            $this->Comment->create();
            $this->request->data['Comment']['user_id'] = $this->Auth->user('id');
            if(!empty($this->data))
                {
                    //Check if image has been uploaded
                    if(!empty($this->request->data['Comment']['image']['name']))
                    {
                        $file = $this->request->data['Comment']['image']; //put the data into a var for easy use

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                        $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions

                        //only process if the extension is valid
                        if(in_array($ext, $arr_ext))
                        {
                            //do the actual uploading of the file. First arg is the tmp name, second arg is
                            //where we are putting it
                            move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/' . $file['name']);

                            //prepare the filename for database entry
                            $this->request->data['Comment']['image'] = '/img/'.$file['name'];
                        }
                    }else{$this->request->data['Comment']['image'] = null;}
                }        
            if ($this->Comment->save($this->request->data)) {
                $this->Flash->success(__('Your comment has been saved.'));
                return $this->redirect(array('controller' => 'topics', 'action' => 'view/'.$this->request->data['Comment']['topic_id']));
            }

            $this->Flash->error(__('Unable to add your post.'));
        }
        
    }
 public function edit_comment($id =null) {
    if (!$id) {
        throw new NotFoundException(__('Invalid post'));
    }

    $comments = $this->Comment->findById($id);
    if (!$comments) {
        throw new NotFoundException(__('Invalid post'));
    }

    if ($this->request->is(array('post', 'put'))) {
        $this->Comment->id = $id;
        if(!empty($this->data))
                {
                    //Check if image has been uploaded
                    if(!empty($this->request->data['Comment']['image']['name']))
                    {
                        $file = $this->request->data['Comment']['image']; //put the data into a var for easy use

                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                        $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions

                        //only process if the extension is valid
                        if(in_array($ext, $arr_ext))
                        {
                            //do the actual uploading of the file. First arg is the tmp name, second arg is
                            //where we are putting it
                            move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/' . $file['name']);

                            //prepare the filename for database entry
                            $this->request->data['Comment']['image'] = '/img/'.$file['name'];
                        }
                    }else{$this->request->data['Comment']['image'] = null;}
                }        
        if ($this->Comment->save($this->request->data)) {
            $this->Flash->success(__('Your post has been updated.'));
            return $this->redirect(array('controller' => 'topics', 'action' => 'view/'.$comments['Comment']['topic_id']));
        }
        $this->Flash->error(__('Unable to update your post.'));
    }

    if (!$this->request->data) {
        $this->request->data = $comments;
    }
}
public function delete_comment($id,$toid=null) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Comment->delete($id)) {
        $this->Flash->success(
            __('The Comment with id: %s has been deleted.', h($id))
        );
    } else {
        $this->Flash->error(
            __('The Comment with id: %s could not be deleted.', h($id))
        );
    }

    return $this->redirect(array('controller' => 'topics', 'action' => 'view',$toid));
}
/*public function isAuthorized($user) {
    // All registered users can add posts
    if ($this->action === 'add') {
        return true;
    }

    // The owner of a post can edit and delete it
    if (in_array($this->action, array('edit', 'delete'))) {
        $topicId = (int) $this->request->params['pass'][0];
        if ($this->Topic->isOwnedBy($topicId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}*/
/*public function image() {
            if ($this->request->is('post')) {
                $this->Comment->create();
                if ($this->Comment->save($this->request->data)) {
                    $this->Session->setFlash(__('The image has been saved.'));
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('The image could not be saved. Please, try again.'));
                }
                
                    }

                    //now do the save
                    $this->Comment->save($this->data) ;
                }
            }

        }*/
}